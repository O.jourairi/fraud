from PIL import Image
import pytesseract as pt
pt.pytesseract.tesseract_cmd = 'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'
from pdf2image import convert_from_path
import numpy as np
import cv2


def extract_boxes(document_path):
    img = Image.open(document_path)
    img = cv2.cvtColor(np.array(img), cv2.COLOR_BGR2GRAY)

    LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    letters = LETTERS.lower()
    Nums = '0123456789'
    total = Nums+LETTERS+letters
    extracted = pt.image_to_boxes(img, lang ='fra').split('\n')
    coordinates = []
    elts = []
    for i in range(len(extracted)):
        if len(extracted[i])>0:
            if extracted[i].split(' ')[0] in total:
                elt = extracted[i].split(' ')[0]
                coordinate = list(map(int,extracted[i].split(' ')[1:-1]))
                coordinates.append(coordinate)
                elts.append(elt)
    return np.array(coordinates), elts



def extract_sizes(document_path):
    coordinates = extract_boxes(document_path)
    coordinates = coordinates
    sizes = coordinates[:,2] - coordinates[:,0] + coordinates[:,3] - coordinates[:,1]
    return sizes

def extract_moments(document_path):
    img = cv2.imread(document_path)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    img = 1.0*np.array(img)
    coordinates = extract_boxes(document_path)
    a,b = (img).shape
    Y1 = a - coordinates[:,3]
    Y2 = a - coordinates[:,1]
    X1 = coordinates[:,0]
    X2 = coordinates[:,2]
    L =[]
    for i in range(len(coordinates)):
         y1, y2, x1, x2 = Y1[i], Y2[i], X1[i], X2[i]
         crop  = img[y1:y2, x1:x2]
         moments = cv2.moments(crop)
         m1, m2, m3 = moments['nu02'], moments['nu11'], moments['nu20']
         L.append([m1,m2,m3])
    return np.array(L)


def text_alignement(document_path):
    pass






