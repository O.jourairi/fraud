from PIL import Image
import pytesseract as pt
pt.pytesseract.tesseract_cmd = 'C:\\Program Files\\Tesseract-OCR\\tesseract.exe'
from pdf2image import convert_from_path
import numpy as np
import cv2


def extract_numbers(image_path, page_index=0):
    img = cv2.imread(image_path)
    a,b,c = np.array(img).shape
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    #img = img.reshape(a,b,1)
    L = pt.image_to_boxes(img, lang ='fra').split('\n')
    Nums = '0123456789'
    charchters = []
    coordinates = []
    Hus = []
    crops =[]
    P =[]
    exter_crops =[]
    D = {}
    D3 ={}
    for i in range(len(L)):
        if len(L[i])>0:
            #print(L[i])
            if L[i][0] in Nums:

                coordinate = list(map(int,L[i].split(' ')[1:]))
                y1 = a - coordinate[3]
                y2 = a - coordinate[1]
                x1 = coordinate[0]
                x2 = coordinate[2]

                coordinates.append(coordinate)

                crop = np.array(img)[y1:y2, x1:x2]
                exter_crop = np.array(img)[y1-3:y1, x1:x2]
                #crop = (0,3*crop[:,:,0] + 0,59* crop[:,:,1] + 0,11*crop[:,:2])[1]
                ##Hu = cv2.HuMoments(cv2.moments(1.0* crop/255)).reshape(7,)
                Hu=1
                Hus.append(Hu)
                crops.append(1.0* crop)
                exter_crops.append(exter_crop)
                P.append(L[i][0])
                try:
                    lin = len(D[L[i][0]])
                except:
                    D[L[i][0]] = []
                D[L[i][0]].append(1.0*crop)
  
    sus_crops = []
    diff =[]
   
    for key in list(D.keys()):
        number = D[key]
        D1 = {}
        for crop in number:
            try:
                lin = len(D1[crop.shape])
            except:
                D1[crop.shape] = []
            D1[crop.shape].append(crop)
        D[key] = D1

    D2 = {}
    S=[]
    for key in list(D.keys()):
        number  = D[key]
        D2[key] = {}

        for key1 in list(number.keys()):
            K = number[key1]
            m = len(K)
            if m>=2:
                l,p = key1
                q=1
                for i in range(m-1):
                    for j in range(i+1, m):
                        try:
                            lin = len(D2[key][key1])
                        except:
                            D2[key][key1] = []
                        r = np.linalg.norm(K[i]-K[j])/(255*l*p*q)
                        D2[key][key1].append(r)
                        if r<5e-4:
                            S.append([K[i],K[j]])
                            



            
        

    return img, coordinates,  crops, Hus, diff, sus_crops,P, exter_crops, D2,D, S
