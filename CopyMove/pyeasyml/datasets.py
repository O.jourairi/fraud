import requests
import os
from datetime import datetime
import json
import urllib
import base64
import shutil

import owncloud
import pytesseract
from pdf2image import convert_from_bytes, convert_from_path, pdfinfo_from_path
from pytesseract import Output
from spellchecker import SpellChecker

from . import featuresExtraction
from . import fileProcessing
from . import image_preprocessing as ip
from . import nexus
from . import deskewAndRotate as dr

from PyPDF2 import PdfFileWriter, PdfFileReader

import numpy as np
import pandas as pd

import joblib

import multiprocessing
import pikepdf

import hashlib
import zlib

import uuid
from PIL import Image
import PIL.Image
import cv2

import multiprocessing as mp

import gc

from requests.exceptions import ConnectionError

import redis
import gc

import pika
from pathlib import Path
import boto3
import time
import datetime
from botocore.client import Config

import fitz

from doccano_client import DoccanoClient
import json
import shutil
from .services.rabbitmq import ServiceRabbitMQ


##############################################################################

nextcloud_user = 'a.rouillon'
nextcloud_password = 'EasyChain1!'
nextcloud_remote_url = 'https://drive.connect-credit.net'
lblo_url = 'https://easyfile-v3-lblo-dev.easychain.fr'
ml_user_login = 'user-ml'
ml_user_password = 'aPbaYjNwQjArQ3x3th9o0zp'
redis_host = 'redis-ml.redis-ml.svc.cluster.local'
redis_pass = 'EasyChain1!'
redis_port = '6379'
redis_db = '1'

rabbitmq_user = "admin"
rabbitmq_psw = "EasyChain1!"
rabbitmq_host = "rabbitmq-ml.rabbitmq-ml.svc.cluster.local"
pdf2image_queue = "pdf2image"
lblo_queue = "lblo"

s3_region = 'eu-west-3'
s3_access_key = 'minioadmin'
s3_secret_key = 'iHybVjoxyrVu'
s3_bucket = 'classif-images-ml'
s3_endpoint = 'https://minio.ml.bewai.tech'

doccano_endpoint = 'https://doccano.bewai.tech/'
doccano_username='admin'
doccano_password='1O6ZSAdLdK5H'


"""
-----------------------------------------------------------------
Desc:   convert pdf doc to dataframe and push to nexus
Input:  path
        group
        artifact
        version
Output: status   

Traitements appliqués : 
- n/a 
-----------------------------------------------------------------
""" 
def pdf_rotate(pdf_file):

    pdf_writer = PdfFileWriter()
    
    with open(pdf_file, mode='rb') as f:
        reader = PdfFileReader(f, strict=False)
        numpages = reader.getNumPages()
        #print('pdf_rotate: nb pages = ' + str(numpages))
        for i in range(numpages):
            rotation = reader.getPage(i).get('/Rotate')
            #print('rotation', rotation)
            page = reader.getPage(i)
            if isinstance(rotation, type(None)) == False:   
                #print('not none')         
                if rotation != 0:
                    page.rotateClockwise(360-rotation)
            
            pdf_writer.addPage(page)   
            
        with open('tmp.pdf', 'wb') as file:
            pdf_writer.write(file)
    f.close()
    os.remove(pdf_file)
    os.rename('tmp.pdf',pdf_file)
    file.close()

"""
-----------------------------------------------------------------
Desc:   convert pdf doc to dataframe and push to nexus
Input:  pdf file

Output: status   

Traitements appliqués : 
- n/a 
-----------------------------------------------------------------
""" 
def ocr_pdf_doc_processing(pdf_file, deskew_model, maxPages, generate_from_document, s3):
    try:
        #align doc
        pdf_rotate(pdf_file)

        images = {}
        output = {}

        path_uuid = str(uuid.uuid4())
        path = "/easyfile/api/" + path_uuid
        Path(path).mkdir(parents=True, exist_ok=True)  

        jpegopt_param = {
            "quality": 100,
            "progressive": True,
            "optimize": True
        }

        #check for native pdf pages
        page_type = {}
        page_type = get_pdf_type(pdf_file)

        ################### parall function: pdftoimage
        queue = mp.Queue()

        for page in range(1, maxPages + 1):
            if page_type['page_' + str(page)] != "native":
                p = mp.Process(target=pdftoimage, args=(pdf_file, page, jpegopt_param, path, deskew_model, s3,)) 
                p.start()
                p.join()

        del queue
        gc.collect()

        for p in Path(path).glob('*.ppm'):
            p.unlink()

        try:
            os.rmdir(path)
        except:
            pass

        for page in range(1, maxPages + 1):
            if page_type['page_' + str(page)] != "native":
                image_artefact = 'dataset_generation_page_' + str(page)
                images['image_'+str(page - 1)] = image_artefact            

        ts = time.time()
        filename = 'records-' + str(ts) + '.txt'
        myfile = open(filename, "w")
        myfile.write("init###init\n")
        myfile.close()

        n = 0
        image_rank = {}
        image_list_ids = []

        def on_rabbit_connect():
            queue_name = lblo_queue
            ServiceRabbitMQ.create_queue(queue_name)

        ServiceRabbitMQ.connect(on_rabbit_connect, rabbitmq_host, rabbitmq_user, rabbitmq_psw, 'lblo_exchanges', 3600)

        for i in images.keys():  
            n = n + 1
            image_id = images[i]
            #image_rank[image_id] = n
            image_rank[image_id] = int(i.split('_')[1])
            image_list_ids.append(image_id)
            #push_ocr_process_request(image_id, channel, lblo_queue)
            push_ocr_process_request(image_id)

        ServiceRabbitMQ.connection.close()

        images['n_images'] = maxPages
        output['n_images'] = maxPages
        if image_list_ids:
            ocr_feedback_listener(image_list_ids, image_rank, (os. getcwd() + '/' + filename), redis_host, redis_pass, redis_port, redis_db)

            #compile intermediate result
            print('get data from filename: ' + filename)
            with open(filename, 'r') as f:
                while True:
                    line = f.readline()
                    if not line:
                        break        
                    idx = line.split('###')[0]
                    rec = line.split('###')[1]
                    if not idx == "init":
                        if not generate_from_document:
                            output[idx] = rec
                        else:
                            #get document artefact generated by lblo and retrieve textlines
                            document_artefact = 'dataset_generation_page_' + str(int(idx.split('_')[1]) + 1) + '_document_object'
                            s3.download_file(Filename=document_artefact + '.pkl', Bucket=s3_bucket, Key=document_artefact)
                            txt = joblib.load(document_artefact + '.pkl')
                            doc_lines = ''
                            for txtline in txt['textlines']:
                                line = ''
                                for item in txtline:
                                    line += item[1] + ' '
                                doc_lines += line.encode('utf-8').decode('utf-8') + '\n'

                            output[idx] = doc_lines


        #include native pdf pages with text
        with fitz.open(pdf_file) as doc:
            i = 0 
            for page in doc:
                i = i + 1
                if page_type['page_' + str(i)] == "native":
                    text = page.get_text().strip()
                    output['text_' + str(i - 1)] = text

        print('ocr_extract_from_imgs: output')
        print(output)


    except Exception as e:
        print(str(e))
        output['n_images'] = 0

    return output


"""
-----------------------------------------------------------------
Desc:   identifiy native pdf pages
Input:  file
Output: page type dictionary 

Traitements appliqués : 
- n/a 
-----------------------------------------------------------------
""" 
def get_pdf_type(file_name):
    page_type = {}
    index = 0
    for page in fitz.open(file_name):
        index = index + 1
        txt = page.get_text().strip()
        page_type['page_' + str(index)] = "native"
        if len(txt) == 0:
            page_type['page_' + str(index)] = "scan"
        if "docusign" in txt.lower():
            page_type['page_' + str(index)] = "docusign"
        if len(txt) != 0 and ord(txt[0]) == 65533:
            page_type['page_' + str(index)] = "font_embedded"
        if len(txt) != 0 and len(txt.split()) < 50:
            page_type['page_' + str(index)] = "too_few"

    return page_type

"""
-----------------------------------------------------------------
Desc:   convert pdf doc to dataframe and push to nexus
Input:  path
        group
        artifact
        version
Output: status   

Traitements appliqués : 
- n/a 
-----------------------------------------------------------------
""" 
def generate_corpus_from_pdf_dox(dir_path, group, artifact, version, force_recreate, generate_from_document=False):

    #convert pdf document to array
    s3 = boto3.client("s3", endpoint_url=s3_endpoint, region_name=s3_region, aws_access_key_id=s3_access_key, aws_secret_access_key=s3_secret_key, config=Config(signature_version='s3v4'))

    print('generate_corpus_from_pdf_dox: in')
    """
    if "Base d'apprentissage" not in dir_path:
        dir_path = "/Base d'apprentissage/" + dir_path
    """
    print('generate_corpus_from_pdf_dox: dir_path = ' + dir_path)
    print('generate_corpus_from_pdf_dox: group = ' + group)
    print('generate_corpus_from_pdf_dox: artifact = ' + artifact)
    print('generate_corpus_from_pdf_dox: version = ' + version)
    print('generate_corpus_from_pdf_dox: force = ' + force_recreate)
    print('generate_corpus_from_pdf_dox: force = ' + generate_from_document)
    
    nexus.pull_from_nexus('models','four-angles', 'latest')
    model = joblib.load('model.joblib')

    #get latest dataset (latest if exists, if not last if exists) 
    asset_version = nexus.get_artifact_gav('datasets', artifact)

    #retrieve last dataset if it exists
    if not "unknown" in asset_version:
        nexus.pull_from_nexus('datasets', artifact, asset_version)
        os.rename('dataset.pkl', 'latest_dataset.pkl')
        latest_dataframe = joblib.load(open('latest_dataset.pkl','rb'))

    oc = owncloud.Client(nextcloud_remote_url)
    oc.login(nextcloud_user, nextcloud_password)

    dirs = oc.list(dir_path)
    dataset = []
    files = [f for f in dirs if not f.is_dir()]

    if generate_from_document == "true":
        generate_from_document = True

    for f in files:
        print('file: ' + f.get_name())

        file_ext = f.get_name().split('.')[-1]

        if file_ext.lower() == "pdf":
            file_path = dir_path + '/' + f.get_name()
            oc.get_file(file_path, 'ocr_file_tmp.pdf')

            with open('ocr_file_tmp.pdf', 'rb') as afile:
                data = afile.read()
                file_crc = zlib.crc32(data)

            print('---- crc: ' + str(file_crc))
            
            pdf = pikepdf.open('ocr_file_tmp.pdf')
            pdf.save('ocr_file.pdf')
            pdf.close()

            info = pdfinfo_from_path('ocr_file.pdf')
            maxPages = info["Pages"]

            os.remove('ocr_file_tmp.pdf')

            #compare current sha1 with same file in existing dataframe
            registered_file_crc = ""
            if force_recreate == 'false':
                if not "unknown" in asset_version:
                    print('---- existing dataset')
                    if "crc" in latest_dataframe.columns:
                        print('---- crc column in existing dataset')
                        dataframe_files = pd.unique(latest_dataframe['file'])
                        for registered_file in dataframe_files:
                            if registered_file in f.get_name():
                                crc = latest_dataframe.loc[latest_dataframe['file'].isin([registered_file])]
                                v = pd.unique(crc['crc'])
                                registered_file_crc = v[0]
                                break

                if registered_file_crc == file_crc:
                    print('---- retrieve existing data')
                    #we keep file existing data
                    registered_file_data = latest_dataframe.loc[latest_dataframe['file'].isin([f.get_name()])]
                    for i, d in registered_file_data.iterrows():
                        dataset.append([f.get_name(), d.page, d.text, d.crc])
                else:
                    print('---- ocr')
                    ocr_output = ocr_pdf_doc_processing('ocr_file.pdf', model, maxPages, generate_from_document, s3)

                    #loop over ocr_output to concatenate the whole document
                    final_ocr_str = ""
                    #print('looping')
                    for k in range(ocr_output['n_images']):
                        #print('ocr page output: ' + ocr_output['text_' + str(k)])
                        #final_ocr_str = final_ocr_str + ' ' + ocr_output['text_' + str(k)]
                        dataset.append([f.get_name(), k, ocr_output['text_' + str(k)], file_crc])
            else:
                ocr_output = ocr_pdf_doc_processing('ocr_file.pdf', model, maxPages, generate_from_document, s3)

                #loop over ocr_output to concatenate the whole document
                final_ocr_str = ""
                #print('looping')
                for k in range(ocr_output['n_images']):
                    #print('ocr page output: ' + ocr_output['text_' + str(k)])
                    #final_ocr_str = final_ocr_str + ' ' + ocr_output['text_' + str(k)]
                    dataset.append([f.get_name(), k, ocr_output['text_' + str(k)], file_crc])


    #create dataframe
    df = pd.DataFrame(dataset, columns=['file', 'page', 'text', 'crc'])

    #save dataframe
    dirpath = 'target'
    if os.path.exists(dirpath) and os.path.isdir(dirpath):
        shutil.rmtree(dirpath)    
    os.mkdir('target')
    joblib.dump(df,'target/dataset.pkl')

    #push dataframe to nexus
    nexus.push_to_nexus('target', group, artifact, version)    


"""
-----------------------------------------------------------------
Desc:   tag dataset as latest
Input:  dataset name
        dataset version
Output: n/a   

Traitements appliqués : 
- n/a 
-----------------------------------------------------------------
""" 
def tag_dataset_as_latest(artifact, version, env):

    #pull artifact from nx
    downloaded_files = nexus.pull_from_nexus('datasets', artifact, version)

    #create target to push
    try:
        os.stat('target')
    except:
        os.mkdir('target')   

    #move current dataset to target
    for f in downloaded_files:
        os.rename(f, 'target/' + f)

    #generate dataset version indicator
    with open('target/version.json', 'w') as f:
        f.write('{"version":"' + version + '"}' + "\n")

    nexus.push_to_nexus('target', 'datasets', artifact, 'latest-' + env)

"""
-----------------------------------------------------------------
Desc:   get INSEE dataset
Input:  n/a
Output: insee files   

Traitements appliqués : 
- n/a 
-----------------------------------------------------------------
""" 
def get_insee_datasets():

    # get insee data 
    downloaded_files = nexus.pull_from_nexus('datasets', 'insee', 'latest')   

    return downloaded_files

def push_ocr_process_request(image_id):

    #publish message to queue
    msg = '{"image_id":"' + image_id + '", "use_deskew" : "false", "is_native_pdf":"false"}'

    ServiceRabbitMQ.publish(
        lblo_queue, msg, is_json=False, in_thread=False, headers={"key": "dummy_dummy_dummy"})


def ocr_feedback_listener(image_list_ids, image_id_ranks, file_path, redis_host, redis_pass, redis_port, redis_lblo_db):

    print('ocr_feedback_listener')
    print('-----------------------------------')
    print('image_list_ids:')
    print(str(image_list_ids))
    print('-----------------------------------')
    print('image_id_ranks:')
    print(str(image_id_ranks))

    image_status = {}

    for f in image_list_ids:
        image_status[f] = 'running'

    r = redis.Redis(host=redis_host, port=redis_port, password=redis_pass, db=redis_lblo_db)

    ok = False
    while not ok:
        for image_id in image_list_ids:
            if image_status[image_id] == "running":
                result_bytes = r.get(image_id + '_back')
                if not result_bytes is None:
                    result_txt = result_bytes.decode("utf-8") 
                    if result_txt:
                        #suppression des objets cache
                        #r.delete(image_id)
                        r.delete(image_id + '_back')

                        image_status[image_id] = "done"

                        #rank = image_id_ranks[image_id] - 1
                        rank = image_id_ranks[image_id]
                        rec = str('text_' + str(rank)) + '###' + str(result_txt)

                        myfile = open(file_path, "a")
                        myfile.write(rec + '\n')
                        myfile.close()

        ok = True
        for image_id in image_list_ids:
            if image_status[image_id] == "running":
                ok = False
                break
        
    #force delete of redis channel
    for image_id in image_list_ids:
        r.set(image_id + '_back', '')
        r.delete(image_id + '_back')



def pdftoimage(pdf_file, page_rank, jpegopt_param, path, deskew_model, s3):

    PIL.Image.MAX_IMAGE_PIXELS = 933120000
    MAX_WIDTH = 4500
    max_dpi=450

    img = convert_from_path(pdf_file, dpi=max_dpi,  fmt='jpeg', jpegopt=jpegopt_param, output_folder=path, first_page=page_rank, last_page=page_rank, strict=False )
    img_array = np.array(img[0])

    img_array = img_array[:, :, ::-1].copy()
    (height, width) = img_array.shape[:2]

    #scale down the big image
    if width > MAX_WIDTH:
        scale_rate = MAX_WIDTH/width
        img_array = cv2.resize(img_array, None, fx=scale_rate, fy=scale_rate)

    # deskew image
    img_array_deskewed, deskew_angle, osd_deskew_angle, page_deskew_height, page_deskew_width, page_osd_height, page_osd_width, page_post_osd_height, page_post_osd_width = dr.deskew_predict_rotate(deskew_model['kmeans'], img_array, deskew_model['no_clusters'],
                                                deskew_model['scale'], deskew_model['svm'],
                                                deskew_model['kernel'], deskew_model['im_features'])

    img_array_deskewed = np.array(img_array_deskewed)

    image_artefact = 'dataset_generation_page_' + str(page_rank)

    joblib.dump(img_array_deskewed, image_artefact + '.pkl', compress=3)

    s3.upload_file(Filename=image_artefact + '.pkl', Bucket=s3_bucket, Key=image_artefact)


def create_doccano_project(ner_dataset_artifact, ner_dataset_asset_version, desc):

    client = DoccanoClient(doccano_endpoint)
    client.login(username=doccano_username, password=doccano_password)

    myproject = client.create_project(name=ner_dataset_artifact + '-V' + ner_dataset_asset_version, project_type='SequenceLabeling', description=desc, collaborative_annotation=True)
    myproject_json = json.loads(myproject.json())
    myproject_id = myproject_json['id']

    #add members
    client.add_member(project_id=myproject_id, username='f.cardineau', role_name='project_admin')
    client.add_member(project_id=myproject_id, username='v.dorard', role_name='annotation_approver')
    client.add_member(project_id=myproject_id, username='h.nam', role_name='project_admin')
    client.add_member(project_id=myproject_id, username='a.rouillon', role_name='project_admin')
    client.add_member(project_id=myproject_id, username='t.debaere', role_name='annotator')

    #add labels to project
    client.create_label_type(myproject_id, 'span', text='buyer_fullname',	prefix_key=None, suffix_key='a', color='#FF0000')
    client.create_label_type(myproject_id, 'span', text='property_address',	prefix_key=None, suffix_key='b', color='#ff7300')
    #client.create_label_type(myproject_id, 'span', text='property_zipcode',	prefix_key=None, suffix_key='c', color='#ff7300')
    #client.create_label_type(myproject_id, 'span', text='property_city',	prefix_key=None, suffix_key='d', color='#ff7300')
    client.create_label_type(myproject_id, 'span', text='cadastral_number',	prefix_key=None, suffix_key='e', color='#b7ff00')
    client.create_label_type(myproject_id, 'span', text='lot_number',	prefix_key=None, suffix_key='f', color='#b7ff00')
    client.create_label_type(myproject_id, 'span', text='selling_price',	prefix_key=None, suffix_key='g', color='#00ffea')
    client.create_label_type(myproject_id, 'span', text='notary_fees',	prefix_key=None, suffix_key='h', color='#004cff')
    client.create_label_type(myproject_id, 'span', text='trading_costs',	prefix_key=None, suffix_key='i', color='#ea00ff')
    client.create_label_type(myproject_id, 'span', text='trading_costs_payer',	prefix_key=None, suffix_key='j', color='#FDA1FF')
    client.create_label_type(myproject_id, 'span', text='energy_diagnostic_score',	prefix_key=None, suffix_key='k', color='#16A5A5')
    client.create_label_type(myproject_id, 'span', text='sales_agreement_signature_date',	prefix_key=None, suffix_key='l', color='#9F0500')
    client.create_label_type(myproject_id, 'span', text='suspensive_conditions_fulfilment_date',	prefix_key=None, suffix_key='m', color='#68CCCA')
    client.create_label_type(myproject_id, 'span', text='loan_applications_submission_date',	prefix_key=None, suffix_key='o', color='#ea00ff')
    client.create_label_type(myproject_id, 'span', text='signature_date',	prefix_key=None, suffix_key='p', color='#FCC400')
    client.create_label_type(myproject_id, 'span', text='loan_amount',	prefix_key=None, suffix_key='q', color='#aa00ff')
    client.create_label_type(myproject_id, 'span', text='loan_maximum_interest_rate',	prefix_key=None, suffix_key='r', color='#aa00ff')
    client.create_label_type(myproject_id, 'span', text='loan_duration',	prefix_key=None, suffix_key='s', color='#aa00ff')
    client.create_label_type(myproject_id, 'span', text='loan_maximum_monthly_expense',	prefix_key=None, suffix_key='t', color='#aa00ff')
    client.create_label_type(myproject_id, 'span', text='notary_fullname',	prefix_key=None, suffix_key='u', color='#68BC00')
    client.create_label_type(myproject_id, 'span', text='notary_address',	prefix_key=None, suffix_key='v', color='#68BC00')

    return client, myproject_id

def init_ner_labelization_dataset(textual_dataset_artifact, textual_dataset_asset_version, ner_dataset_artifact, ner_dataset_asset_version, desc):

    #get latest dataset (latest if exists, if not last if exists) 
    asset_version = nexus.get_artifact_gav('datasets', textual_dataset_artifact)

    #retrieve last dataset if it exists
    if not "unknown" in asset_version:
        if textual_dataset_asset_version in asset_version:
            textual_dataset_asset_version = asset_version 
        else:
            print('textual dataset ' + textual_dataset_artifact + ' with version ' + textual_dataset_asset_version + ' was not found')
            print('ABORTING')
            return
    else:
        print('textual dataset ' + textual_dataset_artifact + ' with version ' + textual_dataset_asset_version + ' was not found')
        print('ABORTING')
        return

    #pull textual dataset
    nexus.pull_from_nexus('datasets', textual_dataset_artifact, asset_version)
    os.rename('dataset.pkl', 'latest_dataset.pkl')
    latest_dataframe = joblib.load(open('latest_dataset.pkl','rb'))

    client, myproject_id = create_doccano_project(ner_dataset_artifact, ner_dataset_asset_version, desc)

    for i, rec in latest_dataframe.iterrows():

        #add examples to project
        mymeta = {}
        mymeta['doc'] = rec.file
        mymeta['page'] = rec.page

        client.create_example(project_id=myproject_id, text=rec.text, meta=mymeta)

#push labelized dataset to nexus
def push_ner_labelized_dataset(project_name, artifact, version):
    client = DoccanoClient(doccano_endpoint)
    client.login(username=doccano_username, password=doccano_password)

    projects = client.list_projects()
    for item in projects:
        rec = json.loads(item.json())
        if rec['name'] == project_name:
            file_path = client.download(project_id=rec['id'], format='JSONL', only_approved=True)

            dirpath = 'target'
            if os.path.exists(dirpath) and os.path.isdir(dirpath):
                shutil.rmtree(dirpath)    
            os.mkdir('target')

            shutil.unpack_archive(str(file_path), 'target')

            #push dataset file to nexus
            nexus.push_to_nexus('target', 'datasets', artifact, version)    


#init new labelization project from existing one
def init_differential_ner_labelization_dataset(textual_dataset_artifact, textual_dataset_asset_version, ner_dataset_artifact, ner_dataset_asset_version, doccano_project, doccano_project_version, desc):

    #retrieve last textual dataset if it exists
    #get latest dataset (latest if exists, if not last if exists) 
    asset_version = nexus.get_artifact_gav('datasets', textual_dataset_artifact)
    if not "unknown" in asset_version:
        if textual_dataset_asset_version in asset_version:
            textual_dataset_asset_version = asset_version 
        else:
            print('textual dataset ' + textual_dataset_artifact + ' with version ' + textual_dataset_asset_version + ' was not found')
            print('ABORTING')
            return
    else:
        print('textual dataset ' + textual_dataset_artifact + ' with version ' + textual_dataset_asset_version + ' was not found')
        print('ABORTING')
        return

    #pull textual dataset
    nexus.pull_from_nexus('datasets', textual_dataset_artifact, asset_version)
    os.rename('dataset.pkl', 'latest_dataset.pkl')
    latest_dataframe = joblib.load(open('latest_dataset.pkl','rb'))

    #retrieve last ner dataset if it exists
    #get latest dataset (latest if exists, if not last if exists) 
    ner_existing_dataset_asset_version = nexus.get_artifact_gav('datasets', ner_dataset_artifact)
    if not "unknown" in ner_existing_dataset_asset_version:
        if ner_dataset_asset_version in ner_existing_dataset_asset_version:
            ner_dataset_asset_version = ner_existing_dataset_asset_version 
        else:
            print('ner dataset ' + ner_dataset_artifact + ' with version ' + ner_dataset_asset_version + ' was not found')
            print('ABORTING')
            return
    else:
        print('ner dataset ' + ner_dataset_artifact + ' with version ' + ner_dataset_asset_version + ' was not found')
        print('ABORTING')
        return

    nexus.pull_from_nexus('datasets', ner_dataset_artifact, ner_existing_dataset_asset_version)

    client, myproject_id = create_doccano_project(doccano_project, doccano_project_version, desc)

    #upload existing labellisation project
    client.upload(project_id=myproject_id, file_paths=['all.jsonl'], task='SequenceLabeling', format='JSONL')

    #check already labeled records and remove useless id in metadata
    list_examples = client.list_examples(myproject_id)
    for item in list_examples:
        item_json = json.loads(item.json())
        client.update_example_state(myproject_id, item_json['id'])

        meta_dict = item_json['meta']

        mymeta = {}
        mymeta['doc'] = meta_dict['doc']
        mymeta['page'] = meta_dict['page']

        client.update_example(myproject_id, item_json['id'], meta=mymeta)

    #generate new documents to be labellised
    #we only take documents that are not part of the validated ner dataset
    dox = {}
    f = open("all.jsonl", "r")
    for x in f:
        x_json = json.loads(x)
        dox[x_json['doc'] + '#' + str(x_json['page'])] = 0

    for i, rec in latest_dataframe.iterrows():
        if not rec.file + '#' + str(rec.page) in dox.keys():
            #add examples to project
            mymeta = {}
            mymeta['doc'] = rec.file
            mymeta['page'] = rec.page

            client.create_example(project_id=myproject_id, text=rec.text, meta=mymeta)

