from . import nexus
import os

"""
-----------------------------------------------------------------
Desc:   tag model as latest
Input:  model name
        model version
Output: n/a   

Traitements appliqués : 
- n/a 
-----------------------------------------------------------------
""" 
def tag_model_as_latest(artifact, version, env):

    #pull artifact from nx
    downloaded_files = nexus.pull_from_nexus('models', artifact, version)

    #create target to push
    try:
        os.stat('target')
    except:
        os.mkdir('target')   

    #move current model to target
    for f in downloaded_files:
        os.rename(f, 'target/' + f)

    #generate dataset version indicator
    with open('target/version.json', 'w') as f:
        f.write('{"version":"' + version + '"}' + "\n")

    nexus.push_to_nexus('target', 'models', artifact, 'latest-' + env)