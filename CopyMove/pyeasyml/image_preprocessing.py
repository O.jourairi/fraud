import re
import cv2
import numpy as np
import pytesseract
import pandas as pd
import numpy as np
from skimage.color import rgb2gray
from deskew import determine_skew
from . import deskewAndRotate as dr

def get_angle(img):
    """
    Get the angle correction necessary to reorient the text
    """
    osd = pytesseract.image_to_osd(img)
    angle = re.search('(?<=Rotate: )\d+', osd).group(0)
    angle = float(angle)
    if angle > 0:
        angle = 360 - angle
    return angle


def rotate_and_scale(img, angle, scale_factor=0.5):
    """
    To reorient and rescale our image
    """
    (h, w) = img.shape[:2]
    center = (w / 2, h / 2)
    M = cv2.getRotationMatrix2D(center, angle, scale=scale_factor)  # rotate about center of image.

    # choose a new image size.
    new_w, new_h = w * scale_factor, h * scale_factor

    # to prevent corners being cut off
    r = np.deg2rad(angle)
    new_w, new_h = (
        abs(np.sin(r) * new_h) + abs(np.cos(r) * new_w), abs(np.sin(r) * new_w) + abs(np.cos(r) * new_h))

    # warpAffine function call :
    # 1. apply the M transformation on each pixel of the original image
    # 2. save everything that falls within the upper-left "dsize" portion of the resulting image.

    # translation that moves the result to the center of that region.
    (tw, th) = ((new_w - w) / 2, (new_h - h) / 2)
    M[0, 2] += tw  # third column of matrix holds translation, which takes effect after rotation.
    M[1, 2] += th

    rotated_img = cv2.warpAffine(img, M, dsize=(int(new_w), int(new_h)))
    return rotated_img

def apply_reorientation(img):
    img = np.array(img)
    angle = get_angle(img)
    rotated_img = rotate_and_scale(img, angle)
    return rotated_img

def image_deskew(image):
    #image = cv2.imread(image)
    # convert the image to grayscale and flip the foreground
    # and background to ensure foreground is now "white" and
    # the background is "black"
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.bitwise_not(gray)
    # threshold the image, setting all foreground pixels to
    # 255 and all background pixels to 0
    thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    # grab the (x, y) coordinates of all pixel values that
    # are greater than zero, then use these coordinates to
    # compute a rotated bounding box that contains all
    # coordinates
    coords = np.column_stack(np.where(thresh > 0))
    angle = cv2.minAreaRect(coords)[-1]
    # the `cv2.minAreaRect` function returns values in the
    # range [-90, 0); as the rectangle rotates clockwise the
    # returned angle trends to 0 -- in this special case we
    # need to add 90 degrees to the angle
    if angle < -45:
        angle = -(90 + angle)
    # otherwise, just take the inverse of the angle to make
    # it positive
    else:
        angle = -angle
    
    # rotate the image to deskew it
    (h, w) = image.shape[:2]
    center = (w // 2, h // 2)
    M = cv2.getRotationMatrix2D(center, angle, 1.0)
    rotated = cv2.warpAffine(image, M, (w, h), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)
    # draw the correction angle on the image so we can validate it
    #cv2.putText(rotated, "Angle: {:.2f} degrees".format(angle),
    #    (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
    # show the output image
    #print("[INFO] angle: {:.3f}".format(angle))
    #cv2.imshow("Input", image)
    #cv2.imshow("Rotated", rotated)
    #cv2.waitKey(0)
    return rotated

def image_rotation(img):

    grayscale = rgb2gray(img)
    angle = determine_skew(grayscale)
    print(angle)
    (h, w) = img.shape[:2]
    center = (w / 2, h / 2)
    #rotated = rotate(img, angle, resize=True) * 255
    M = cv2.getRotationMatrix2D(center, angle, scale=1.0)
    rotated = cv2.warpAffine(img, M, (w, h))
    print('type after rotated',type(rotated))
    return rotated


# get grayscale image
def get_grayscale(image):
    return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

def preprocess_image(image, four_angles_models, use_deskew):
    # Reorient image with appropriate scaling
    #img = Preprocessing.apply_reorientation(image)
    #img = Preprocessing.image_deskew(image)
    #img = Preprocessing.image_rotation(image)
    img = image
    if use_deskew:
        img = dr.deskew_predict_rotate(four_angles_models['kmeans'], image, four_angles_models['no_clusters'],
                                    four_angles_models['scale'],four_angles_models['svm'],
                                    four_angles_models['kernel'],four_angles_models['im_features'])

    #return img

    # Turns image to grayscale
    img = get_grayscale(img)
    # Apply dilation and erosion to remove some noise
    kernel = np.ones((1, 1), np.uint8)
    img = cv2.dilate(img, kernel, iterations=1)
    img = cv2.erode(img, kernel, iterations=1)
    # Apply filter with threshold
    cv2.adaptiveThreshold(cv2.GaussianBlur(img, (5, 5), 0), 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,
                            31, 2)

    return img