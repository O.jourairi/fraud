import numpy as np
from random import randint
import os
import json
import pickle
import joblib
import nltk.data
from numpy import mean
from numpy import std
from sklearn.svm import LinearSVC
from gensim import corpora, matutils
from sklearn.metrics import classification_report
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
import spacy
import pandas as pd
from . import fileProcessing as fp
from . import nlp as nlp

class FeatureExtraction(object):
    def __init__(self, data):
        self.data = data

    def build_dictionary(self, cloud, dictionary_name, url):
        dict_words = []
        i = 0
        for text in self.data:
            i += 1
            words = nlp.NLP(text = text['content']).get_words_feature()
            dict_words.append(words)

        fp.FileStore(cloud, dictionary_name, url).store_dictionary(dict_words)

    def __load_dictionary(self, dictionary_tmp_file):
        self.dictionary = corpora.Dictionary.load_from_text(dictionary_tmp_file)

    def __build_dataset(self, dictionary_tmp_file):
        self.features = []
        self.labels = []
        i = 0
        for d in self.data:
            i += 1
            self.features.append(self.get_dense(d['content'], dictionary_tmp_file))
            self.labels.append(d['category'])

    def get_dense(self, text, dictionary_tmp_file):
        self.__load_dictionary(dictionary_tmp_file)
        words = nlp.NLP(text).get_words_feature()
        # Bag of words
        vec = self.dictionary.doc2bow(words)
        dense = list(matutils.corpus2dense([vec], num_terms=len(self.dictionary)).T[0])
        return dense

    def get_data_and_label(self, dictionary_tmp_file):
        self.__build_dataset(dictionary_tmp_file)
        return self.features, self.labels
    
    def get_data_and_label_tfidf(self):
        self.features = []
        self.labels = []
        i = 0
        for d in self.data:
            i += 1
            print("Step {} / {}".format(i, len(self.data)))
            self.features.append(d['content'])
            self.labels.append(d['category'])
        return self.features, self.labels
    
    def get_vector_with_NLP(self):       
        self.features = []
        self.labels = []
        i = 0
        for d in self.data:
            i += 1
            print("Step {} / {}".format(i, len(self.data)))
            words = nlp.NLP(d['content']).get_words_feature()
            tmp = ' '.join(words)
            self.features.append(tmp)
            self.labels.append(d['category'])

        return self.features, self.labels
    
    def get_vector(self):       
        self.features = []
        self.labels = []
        i = 0
        for d in self.data:
            i += 1
            print("Step {} / {}".format(i, len(self.data)))
            self.features.append(d['content'])
            self.labels.append(d['category'])

        return self.features, self.labels

    def preprocessing(self):
        
            self.features = []
            self.labels = []
            self.typeDoc = []
            i = 0
            for d in self.data:
                i += 1
                print("Step {} / {}".format(i, len(self.data)))
                words = nlp.NLP(d['content']).get_words_feature()
                tmp = ' '.join(words)
                self.features.append(tmp)
                self.labels.append(d['category'])
                self.typeDoc.append(d['type'])
        #print(self.features)
        #print(self.labels)
            return self.features, self.labels, self.typeDoc
