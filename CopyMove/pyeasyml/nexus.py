import requests
import os
from datetime import datetime
import json
import urllib
import base64
import shutil


"""
-----------------------------------------------------------------
Desc:   push artifacts to nexus
Input:  path to artifacts
        group
        artifact
        version
Output: status   

Traitements appliqués : 
- n/a 
-----------------------------------------------------------------
"""
def push_to_nexus(target, group, artifact, version):
    now = datetime.now()
    unik = now.strftime("%Y%m%d%H%M%S")
    
    for f in os.listdir(target):
        #print(f)
        file_ext = f.split('.')[-1]
        file_name = f
        if "." in f:
            file_name = f.replace('.' + file_ext, '')

        artifact_file = target + '/' + f 
        if 'latest-' not in version:
            if "." in f:
                artifact_name = file_name + '-' + version + '-' + unik + '.' + file_ext
            else:
                artifact_name = file_name + '-' + version + '-' + unik
            url = 'https://nexus.bewai.tech/repository/' + group + '/easychain/' + artifact + '/' + version + '-' + unik + '/' + artifact_name
        else:
            if "." in f:                
                artifact_name = file_name + '-' + version + '.' + file_ext
            else:
                artifact_name = file_name + '-' + version
            url = 'https://nexus.bewai.tech/repository/' + group + '/easychain/' + artifact + '/' + version + '/' + artifact_name

        upload_data = open(target + '/' + f, "rb")
        
        response = requests.request("PUT", url, data=upload_data, auth=('admin', 'EasyChain1!'))  
        
        upload_data.close()
        
        if response.status_code != 201:
            print(file_name)
            print("Error uploading artifact to Nexus: ")
            print('error code: ' + str(response.status_code))
            print('error: ' + response.text)
            return

"""
-----------------------------------------------------------------
Desc:   pull artifacts from nexus
Input:  group
        artifact
        version
Output: status   

Traitements appliqués : 
- n/a 
-----------------------------------------------------------------
""" 
def pull_from_nexus(repository, artifact, version):

    url = 'https://nexus.bewai.tech/service/rest/v1/search?repository='+ repository +'&group=%09%2Feasychain%2F' + artifact + '%2F' + version    
    headers = {
      'accept': 'application/json'
    }

    print('pulling from nexus')
    print(url)
    response = requests.request("GET", url, headers=headers, auth=('admin', 'EasyChain1!'))  
    
    print('response: ' + str(response.status_code))
    
    if response.status_code != 200:
        print("Error getting assets list from Nexus: ")
        print('error code: ' + str(response.status_code))
        print('error: ' + response.text)
        return

    print('response text: ' + response.text)

    json_feedback_text = response.text
    json_feedback = json.loads(json_feedback_text)
    
    files_assets = []
    for i in json_feedback['items']:
        print(i)
        for asset in i['assets']:
            nexus_asset_path = asset['path']
            nexus_asset_name = nexus_asset_path.split('/')[1]
            nexus_asset_version = nexus_asset_path.split('/')[2]
            nexus_file_name = nexus_asset_path.split('/')[-1]
            nexus_file_name = nexus_file_name.replace('-' + version,'')
            files_assets.append(nexus_file_name)
            print('nexus_asset_name: ' + nexus_asset_name)
            print('nexus_asset_version: ' + nexus_asset_version)
            url = asset['downloadUrl']
            print('downloadUrl: ' + url)
            
            with requests.get(url, auth=('easychain','EasyChain1!'), stream=True) as r:
                r.raise_for_status()
                with open(nexus_file_name, "wb") as f:
                    for chunk in r.iter_content(chunk_size=1024):
                        f.write(chunk)

    return files_assets

"""
-----------------------------------------------------------------
Desc:   get latest/last artifact gav if it exist
Input:  repository
        artifact
Output: version   

Traitements appliqués : 
- n/a 
-----------------------------------------------------------------
""" 
def get_artifact_gav(repository, artifact):

    url = 'https://nexus.bewai.tech/service/rest/v1/search?repository='+ repository +'&group=%09%2Feasychain%2F' + artifact   
    headers = {
      'accept': 'application/json'
    }

    print('trying to get latest')
    url_latest = url + '%2Flatest'
    print(url_latest)
    response = requests.request("GET", url_latest, headers=headers, auth=('admin', 'EasyChain1!'))  
    
    print('response: ' + str(response.status_code))
    
    if response.status_code != 200:
        print("Error getting assets list from Nexus: ")
        print('error code: ' + str(response.status_code))
        print('error: ' + response.text)
        return

    print('response text: ' + response.text)
    if "latest" in response.text:
        return "latest"

    print('trying to get last')
    url_last = url + '%2F*'
    print(url_last)
    response = requests.request("GET", url_last, headers=headers, auth=('admin', 'EasyChain1!'))  
    
    print('response: ' + str(response.status_code))
    
    if response.status_code != 200:
        print("Error getting assets list from Nexus: ")
        print('error code: ' + str(response.status_code))
        print('error: ' + response.text)
        return

    print('response text: ' + response.text)

    if not artifact in response.text:
        return "unknown"

    json_feedback_text = response.text
    json_feedback = json.loads(json_feedback_text)
    nexus_asset_version = ""
    for i in json_feedback['items']:
        for asset in i['assets']:
            nexus_asset_path = asset['path']
            nexus_asset_version = nexus_asset_path.split('/')[2]
            print('nexus_asset_version: ' + nexus_asset_version)
    return nexus_asset_version
