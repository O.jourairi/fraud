import numpy as np
from random import randint
import os
import json
import pickle
import joblib
import nltk.data
from numpy import mean
from numpy import std
#from utils import Preprocessing
from sklearn.svm import LinearSVC
from gensim import corpora, matutils
from sklearn.metrics import classification_report
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_val_score
import spacy
import pandas as pd

class FileReader(object):
    def __init__(self, filePath, encoder = None):
        self.filePath = filePath
        self.encoder = encoder

    def read(self):
        with open(self.filePath) as f:
            s = f.read()
        return s

    def content(self):
        s = self.read()
        return s.decode(self.encoder)

    def read_json(self):

        with open(self.filePath, encoding = self.encoder) as f:
            s = json.load(f)
        return s

    def read_stopwords(self):
        with open(self.filePath, 'r', encoding = self.encoder) as f:
            stopwords = set([w.strip().replace(' ', '_') for w in f.readlines()])
        return stopwords

    def load_dictionary(self):
        
        return corpora.Dictionary.load_from_text(self.filePath)

class FileStore(object):
    def __init__(self, cloud, fileName, url_output, data = None):
        self.cloud = cloud
        self.fileName = fileName
        self.url_output = url_output
        self.data = data

    def store_json(self):
        with open(self.fileName, 'a+', encoding='utf-8') as outfile:
            json.dump(self.data, outfile, ensure_ascii=False)
        self.cloud.put_file(self.url_output, self.fileName)
        os.remove(self.fileName)
        

    def store_dictionary(self, dict_words):
        dictionary = corpora.Dictionary(dict_words)
        dictionary.filter_extremes(no_below=5)
        dictionary.save_as_text(self.fileName)
        self.cloud.put_file(self.url_output, self.fileName)
        os.remove(self.fileName)
        

    def save_pickle(self,  obj):
        joblib.dump(obj, self.filePath)
