import owncloud
import requests


def connect_to_cloud(user, password, url):
    oc = owncloud.Client(url)
    oc.login(user, password)
    return oc

def request_file(remote_path, user, password, text=True):
    r = requests.request(
        method='get',
        url=remote_path,
        auth=(user, password)
    )
    if text:
        return r.text
    else:
        return r.content