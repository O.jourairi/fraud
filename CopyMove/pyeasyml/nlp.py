import numpy as np
from random import randint
import os
import json
import pickle
import joblib
from numpy import mean
from numpy import std

import spacy
import pandas as pd
from spacy.lang.fr.stop_words import STOP_WORDS as fr_stop
import os

from spacy_langdetect import LanguageDetector
import uuid


"""
def __init__(self, text = None):
    self.text = text
    self.__set_stopwords()

def __set_stopwords(self):
    self.stopwords = list(fr_stop)
"""

def split_words(text):
    nlp = spacy.load('fr_core_news_sm')
    tmp = nlp(text)
    tmp_text = [token.lemma_ for token in tmp]
    try:
        return [x for x in tmp_text]
    except TypeError:
        return []

def get_words_feature(text):
    nlp = spacy.load('fr_core_news_sm')
    tmp = nlp(text)
    stopwords = list(fr_stop)
    tmp_text = []
    for word in tmp:
        #text_tmp = word.text.lower()
        text_tmp = word.text
        if (text_tmp not in stopwords) and (text_tmp.isalpha() == True):
            tmp_text.append(word.lemma_)

    return tmp_text

def get_words_feature_preloaded(text, nlp):
    #nlp = spacy.load('fr_core_news_sm')
    tmp = nlp(text)
    stopwords = list(fr_stop)
    tmp_text = []
    for word in tmp:
        #text_tmp = word.text.lower()
        text_tmp = word.text
        if (text_tmp not in stopwords) and (text_tmp.isalpha() == True):
            tmp_text.append(word.lemma_)

    return tmp_text

def verif_valid_french_ocr_result(text, nlp, threshold):

    #nlp = spacy.load('fr_core_news_md')  # 1
    #pipeline_uuid = str(uuid.uuid4())
    #pipeline_name = 'language_detector_' + pipeline_uuid
    #nlp.add_pipe(LanguageDetector(), name=pipeline_name, last=True) #2
    doc = nlp(text) #3
    #nlp object is instanciated with a pipeline including LanguageDetector 
    detect_language = doc._.language #4
    print(detect_language)
    #nlp.remove_pipe(pipeline_name)

    if detect_language['language'] == 'fr' and float(detect_language['score']) > threshold:
        return True
    else:
        return False
