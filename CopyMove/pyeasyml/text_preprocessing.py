import re
import numpy as np
import pandas as pd
import numpy as np
import nltk
import spacy

from nltk.stem import WordNetLemmatizer
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords

"""
nltk.download('wordnet')
nltk.download('punkt')
nltk.download('stopwords')
"""

def n_frequent_words(content_txt, n=500):
    """Extract the n more frequent words from the training dataset corpus or headers using the training files
    db_corpus.txt or db_head.txt

            **parameters**, **types**, **return** and **return types**::

                    :param n: number of more frequent words
                    :param content_txt: text that we want the more frequent words from
                    :type n: int
                    :type content_txt: string
                    :return: a dictionary with n more frequent words in the chosen content
                    :rtype: dataframe
    """
    tab = np.array(content_txt.split(' '))
    df = pd.DataFrame(tab, columns=['word'])
    tab = [[x, y] for x, y in zip(df['word'].value_counts().keys(), df['word'].value_counts())]
    df2 = pd.DataFrame(tab, columns=['word', 'occurence']).dropna()
    return df2['word'][:n]

def punctuation(text):
    """
    Remove the punctuation from a text

            **parameters**, **types**, **return** and **return types**::

                :param text: text to remove punctuation from
                :type text: string
                :return: the input text without punctuation
                :rtype: string
    """
    punctuation = [',', ":", ";", "==", ".", "(",")","|","&",'"',"'",">","’","“","‘","”","[","]",
                    "/","_","@","\\","!","«","»","{", "}"]
    for p in punctuation:
        text = text.replace(p, " ")

    return text

def abbreviation(text):
    """
    Remove abbreviations from a text

            **parameters**, **types**, **return** and **return types**::

                :param text: text to remove abbreviations from
                :type text: string
                :return: the input text without abbreviations
                :rtype: string
    """
    text = text.replace("j'", "je ")
    text = text.replace("n'", "ne ")
    text = text.replace("t'", "te ")
    text = text.replace("m'", "me ")
    text = text.replace("\n", " ")
    return text

def remove_short_words(text):
    """
    Remove a very short word from a text

            **parameters**, **types**, **return** and **return types**::

                :param text: text to remove short word from
                :type text: string
                :return: the input text without short words
                :rtype: string
    """
    result = []
    exceptions = ['cb']
    for i in text.split(' '):
        if (len(i) >= 3 or (len(i) < 3 and i in exceptions)) :
            result.append(i)
    
    return " ".join(result)


def stopwords_remove(text, language='french'):
    """
    Removes stopwords of the configured language from the text

            **parameters**, **types**, **return** and **return types**::

                :param text: text to remove stopwords from
                :param language: language of the text
                :type text: string
                :type language : string
                :return: the input text without stopwords
                :rtype: string
    """

    lang_stopwords = set(stopwords.words(language))
    token = [token for token in text.lower().split(" ") if token != " " if token not in lang_stopwords]
    text = " ".join(["".join(token) for token in token])
    return text

def preprocessing(text, lex_analysis_method='stem'):
    """
    Preprocess text and perform lexical analysis with the stemmer of lemmatizer method

            **parameters**, **types**, **return** and **return types**::

                :param lex_analysis_method: lexical analysis method : stem or lemmatize
                :param text: text to preprocess
                :type text: string
                :type lex_analysis_method : string
                :return: the preprocessed text
                :rtype: string
    """
    # Simple text standardisation
    text = text.lower()
    text = abbreviation(text)
    text = punctuation(text)
    text = stopwords_remove(text)
    text = remove_short_words(text)
    
    return text

def get_number_of_nums(text):
    """
    Count the number of numeric characters in a text

            **parameters**, **types**, **return** and **return types**::

                :param text: input text
                :type text: string
                :return: the number of numeric characters
                :rtype: int
    """
    count = 0
    digits = [str(i) for i in range(10)]
    for x in text.split(' '):
        count = count + 1 if is_digital_word(x, digits) else count
    return count

def is_digital_word(word, digits):
    """
    Define if a word contains numeric characters

            **parameters**, **types**, **return** and **return types**::

                :param word: input word
                :param digits: numeric characters
                :type word: string
                :type digits: characters array
                :return: true if the word contains numeric characters, false otherwise
                :rtype: bool
    """
    for digit in digits:
        if digit in word:
            return True
    return False

def preprocess_remove_entities(text, stop_words, remove_surnames=True, remove_address=True, remove_names=True, remove_cities=True):
    result = []

    text_rebuild = text
    if remove_surnames: 
        #remove surname
        for token in text.split(' '):
            if token.lower() not in surname_stopwords:
                result.append(token)
        text_rebuild = ' '.join(result)
    
    #remove address
    if remove_address:
        re_adress_pattern = '\d{1,3}(?:\s[a-zA-Z\u00C0-\u017F]+)+'  
        a = re.findall(re_adress_pattern, text_rebuild)
        for item in a:
            if 'avenue' in item or 'rue' in item or 'boulevard' in item:
                text_rebuild = text_rebuild.replace(item, '')    
    
    #remove names reference
    if remove_names:
        re_name_pattern = '(monsieur|madame)((?:\s[a-z]+))'    
        b = re.findall(re_name_pattern, text_rebuild)
        for item in b:
            text_rebuild = text_rebuild.replace(item[0] + item[1], '')

    #remove stopwords and cities
    result = []
    if remove_cities:
        for token in gensim.utils.simple_preprocess(text_rebuild):
            if token.lower() not in stop_words and token.lower() not in cities_stopwords and not len(token) < 3:
                result.append(token)
    else:
        for token in gensim.utils.simple_preprocess(text_rebuild):
            if token.lower() not in stop_words and not len(token) < 3:
                result.append(token)

    return result    
