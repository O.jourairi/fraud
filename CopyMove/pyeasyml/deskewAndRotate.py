import os
import sys
from os import path
from pyeasyml import nexus as nx
import argparse
import cv2
import numpy as np
import os
from PIL import Image
from PIL import ImageOps
from skimage.transform import rotate
from skimage.color import rgb2gray
from deskew import determine_skew
######################################
# from matplotlib import pyplot as plt
from joblib import dump, load

from pdf2image import convert_from_bytes, convert_from_path
import pytesseract
import re

import subprocess
import uuid
import time
import imutils


def point_in_box(x, y, box):
    x_min, y_min, x_max, y_max = box

    in_box_x = x_min <= x <= x_max
    in_box_y = y_min <= y <= y_max

    if in_box_x is True and in_box_y is True:
        return True
    return False


def get_predominant(image):
    grey_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    (height, width) = image.shape[:2]
    x_mid = int(width/2)
    y_mid = int(height/2)
    x_quater = int(width/4)
    y_quater = int(height/4)
    top_left = (0, 0, x_mid, y_mid)
    top_right = (x_mid, 0, width, y_mid)
    bottom_left = (0, y_mid, x_mid, y_mid)
    bottom_right = (x_mid, y_mid, width, height)
    center = (x_quater, y_quater, 3*x_quater, 3*y_quater)

    binary_image = cv2.adaptiveThreshold(
        grey_image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 85, 41)
    analysis = cv2.connectedComponentsWithStats(binary_image, 4, cv2.CV_32S)

    (numLabels, labels, stats, centroids) = analysis

    top_left_chars = 0
    top_right_chars = 0
    bottom_left_chars = 0
    bottom_right_chars = 0
    center_chars = 0
    for i in range(0, numLabels):
        area = stats[i, cv2.CC_STAT_AREA]
        (x, y) = centroids[i]
        (cx, cy) = (int(x), int(y))
        if 50 < area < 300000:

            if point_in_box(cx, cy, top_left):
                top_left_chars = top_left_chars+1

            if point_in_box(cx, cy, top_right):
                top_right_chars = top_right_chars+1

            if point_in_box(cx, cy, bottom_left):
                bottom_left_chars = bottom_left_chars+1

            if point_in_box(cx, cy, bottom_right):
                bottom_right_chars = bottom_right_chars+1

            if point_in_box(cx, cy, center):
                center_chars = center_chars+1

    max_chars = max(top_left_chars, top_right_chars,
                    bottom_left_chars, bottom_right_chars,  center_chars)
    roi = image

    if top_left_chars == max_chars:
        (startX, startY, endX, endY) = top_left
        roi = image[startY:endY, startX:endX]
    elif top_right_chars == max_chars:
        (startX, startY, endX, endY) = top_right
        roi = image[startY:endY, startX:endX]
    elif bottom_left_chars == max_chars:
        (startX, startY, endX, endY) = bottom_left
        roi = image[startY:endY, startX:endX]
    elif bottom_right_chars == max_chars:
        (startX, startY, endX, endY) = bottom_right
        roi = image[startY:endY, startX:endX]
    elif center_chars == max_chars:
        (startX, startY, endX, endY) = center
        roi = image[startY:endY, startX:endX]
    return roi


def deskew(_img):
    image = _img
    # cut a small part for determining the angle
    small_part = get_predominant(image)
    if small_part.shape[0] == 0 and small_part.shape[1] == 0:
        return image, 0
    grayscale = cv2.cvtColor(small_part, cv2.COLOR_BGR2GRAY)

    angle = determine_skew(grayscale)

    if angle is None:
        return image, 0

    #20221129: revert this code as negative angles may give bad result for classification or extraction 
    #if 0.25 < abs(angle) < 359.75:
    if 0.25 < angle < 359.75:
        returned_angle = angle
        rotated = imutils.rotate_bound(image, -angle)
    else:
        returned_angle = 0
        rotated = image
    return rotated, returned_angle


def getDescriptors(img):
    kptDetector = cv2.FastFeatureDetector_create()
    kp = kptDetector.detect(img, None)
    sift = cv2.SIFT_create()
    # desExtractor = cv2.SIFT_create()
    kp, des = sift.compute(img, kp)
    return des


def readImage(img_path):
    # img = cv2.imread(img_path, 0)
    img = Image.open(img_path)
    gray_image = ImageOps.grayscale(img)
    new_image = ImageOps.pad(gray_image, (800, 800), Image.ANTIALIAS)
    return np.array(new_image)


def readImagefromArray(img):
    # img = cv2.imread(img_path, 0)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    im_pil = Image.fromarray(img)
    gray_image = ImageOps.grayscale(im_pil)
    new_image = ImageOps.pad(gray_image, (800, 800), Image.ANTIALIAS)
    return np.array(new_image)


'''
deskew image firstly, then predict the angle of deskewed image and rotate it if the angle is not equal zero
Rotated file is save in the same folder +filename_rotate.extension
'''


def deskew_predict_rotate(kmeans, imgPath, no_clusters, scale, svm, kernel, im_features):
    '''
    if (not path.exists(imgPath)):
        print(imgPath+' is not exist!')
        return
    '''
    (height, width) = imgPath.shape[:2]
    page_osd_height = height
    page_osd_width = width
    page_deskew_height = height
    page_deskew_width = width
    page_post_osd_height = height
    page_post_osd_width = width

    t_start = time.time()
    fileName, deskew_angle = deskew(imgPath)
    t_end = time.time()
    print("deskew_predict_rotate: deskew = " + str(t_end - t_start))

    (height, width) = fileName.shape[:2]
    page_osd_height = height
    page_osd_width = width


    #########################
    #img = readImage(fileName)
    '''
    img = readImagefromArray(fileName)
    print(type(img))
    descriptors = getDescriptors(img)
    vocab = np.array( [[ 0 for i in range(no_clusters)]])
    test_ret = kmeans.predict(descriptors)
    for each in test_ret:
            vocab[0][each] += 1
    vocab=scale.transform(vocab)
    # print(vocab.shape[1])
    kernel_test = vocab
    if(kernel == "precomputed"):
        kernel_test = np.dot(vocab, im_features.T)
    label=svm.predict(kernel_test)
    angle=0
    if label==0:
        angle=0
    elif label==1:
        angle=90
    elif label==2:
        angle=180
    elif label==3:
        angle=270
    print('SVM classify:',angle)
    ##########################"
    '''
    #image_deskew = io.imread(fileName)

    image_deskew = fileName

    # cut a small part for determining the angle
    small_part = get_predominant(image_deskew)
    if small_part.shape[0] == 0 and small_part.shape[1] == 0:
        print('Rotation finished!')
        return image_deskew, 0, 0, page_deskew_height, page_deskew_width, page_osd_height, page_osd_width, page_post_osd_height, page_post_osd_width

    # Using pytesseract.image_to_osd
    #osd_rotated_image = pytesseract.image_to_osd(image_deskew)
    t_start = time.time()
    osd_uuid = str(uuid.uuid4())
    cv2.imwrite(osd_uuid + '.jpg',small_part)
    result = subprocess.run(
        ['tesseract', osd_uuid + '.jpg', osd_uuid, '--psm', '0', '-l', 'fra'], stdout=subprocess.PIPE)
    osd_rotated_image = subprocess.run(
        ['cat', osd_uuid + '.osd'], stdout=subprocess.PIPE).stdout.decode('utf-8')

    try:
        os.remove(osd_uuid + '.jpg')
        os.remove(osd_uuid + '.osd')
    except:
        pass

    try:
        osd_angle = int(re.search('(?<=Rotate: )\d+', osd_rotated_image).group(0))
    except:
        osd_angle = 0
    t_end = time.time()
    print("deskew_predict_rotate: osd = " + str(t_end - t_start))

    '''
    fName=os.path.basename(imgPath)
    name, extension = os.path.splitext(fName)
    basepath=os.path.dirname(imgPath)
    '''
    result = None
    t_start = time.time()
    result = image_deskew
    if osd_angle != 0:
        result = imutils.rotate_bound(image_deskew, osd_angle)
    t_end = time.time()
    print("deskew_predict_rotate: rotate = " + str(t_end - t_start))

    print('Rotation finished!')

    page_post_osd_height, page_post_osd_width = result.shape[:2]

    return result, deskew_angle, osd_angle, page_deskew_height, page_deskew_width, page_osd_height, page_osd_width, page_post_osd_height, page_post_osd_width
    # return np.array(result)

# "
