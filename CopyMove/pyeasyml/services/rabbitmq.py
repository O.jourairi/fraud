"""
RabbitMQ
"""

import os
import threading
import functools
import gc
import json
import time

import pika

from .logger import ServiceLogger


class ServiceRabbitMQ:
    "Manipulate RabbitMQ"

    @staticmethod
    def connect(callback, rabbitmq_host, rabbitmq_user, rabbitmq_psw, queue_exchange, heartbeat_value=5):
        "Connect"
        connection = None
        channel = None

        def on_connect_error():
            ServiceLogger.error('Error on connect to RabbitMQ, try reconnect...')

            try:
                ServiceRabbitMQ.channel.close()
            except:
                pass

            try:
                ServiceRabbitMQ.connection.close()
            except:
                pass

            time.sleep(30)
            ServiceRabbitMQ.connect(callback)

        ServiceRabbitMQ.host = rabbitmq_host
        ServiceRabbitMQ.user = rabbitmq_user
        ServiceRabbitMQ.password = rabbitmq_psw
        ServiceRabbitMQ.exchange = queue_exchange

        try:
            credentials = pika.PlainCredentials(
                ServiceRabbitMQ.user, ServiceRabbitMQ.password)

            parameters = pika.ConnectionParameters(
                ServiceRabbitMQ.host, credentials=credentials, heartbeat=heartbeat_value)

            connection = pika.BlockingConnection(parameters)

            ServiceRabbitMQ.connection = connection

            channel = ServiceRabbitMQ.connection.channel()

            channel.exchange_declare(
                exchange=ServiceRabbitMQ.exchange,
                exchange_type='direct',
                passive=False,
                durable=True,
                auto_delete=False)

            channel.basic_qos(prefetch_count=1)

            ServiceRabbitMQ.channel = channel

            ServiceRabbitMQ.threads = []

        except Exception as e:
            print(str(e))
            on_connect_error()

    @staticmethod
    def create_consumer(callback, rabbitmq_host, rabbitmq_user, rabbitmq_psw, queue_exchange):
        "Connect"
        connection = None
        channel = None

        def on_connect_error():
            ServiceLogger.error('Error on connect to RabbitMQ, try reconnect...')

            try:
                ServiceRabbitMQ.channel.close()
            except:
                pass

            try:
                ServiceRabbitMQ.connection.close()
            except:
                pass

            time.sleep(30)
            ServiceRabbitMQ.connect(callback)

        ServiceRabbitMQ.host = rabbitmq_host
        ServiceRabbitMQ.user = rabbitmq_user
        ServiceRabbitMQ.password = rabbitmq_psw
        ServiceRabbitMQ.exchange = queue_exchange

        try:
            credentials = pika.PlainCredentials(
                ServiceRabbitMQ.user, ServiceRabbitMQ.password)

            parameters = pika.ConnectionParameters(
                ServiceRabbitMQ.host, credentials=credentials, heartbeat=5)

            connection = pika.BlockingConnection(parameters)

            ServiceRabbitMQ.connection = connection

            channel = ServiceRabbitMQ.connection.channel()

            channel.exchange_declare(
                exchange=ServiceRabbitMQ.exchange,
                exchange_type='direct',
                passive=False,
                durable=True,
                auto_delete=False)

            channel.basic_qos(prefetch_count=1)

            ServiceRabbitMQ.channel = channel

            ServiceRabbitMQ.threads = []

            callback()

            ServiceRabbitMQ.channel.start_consuming()

            while True:
                time.sleep(10)
                if not ServiceRabbitMQ.channel.is_open() or not ServiceRabbitMQ.connection.is_open():
                    on_connect_error()
                    break
        except KeyboardInterrupt:
            ServiceRabbitMQ.channel.stop_consuming()
            ServiceRabbitMQ.connection.close()
        except:
            on_connect_error()


    @staticmethod
    def create_queue(queue_name):
        "Declare new queue"

        ServiceRabbitMQ.channel.queue_declare(queue=queue_name, durable=True)

    @staticmethod
    def consume(queue_name, callback, is_json=True):
        "Consume message queue"

        def on_message(ch, method_frame, _header_frame, body):
            del ch
            delivery_tag = method_frame.delivery_tag
            message_headers = _header_frame.headers

            if is_json is True:
                body = json.loads(body.decode("utf-8"))

            thread = threading.Thread(
                target=callback, args=(delivery_tag, body, message_headers,))
            thread.start()
            ServiceRabbitMQ.threads.append(thread)

            del body
            gc.collect()

        ServiceRabbitMQ.channel.queue_bind(queue=queue_name,
                                           exchange=ServiceRabbitMQ.exchange, routing_key="standard_key")

        ServiceRabbitMQ.channel.basic_consume(queue_name, on_message)

    @staticmethod
    def publish(queue_name, content, is_json=True, in_thread=False, headers={}):
        "Publish message queue"

        def publish_function(content, is_json, headers):
            if is_json is True:
                content = json.dumps(content)

            if ServiceRabbitMQ.channel.is_open:
                ServiceRabbitMQ.channel.basic_publish(
                    exchange='', routing_key=queue_name, body=content, properties=pika.BasicProperties(headers=headers))

            del content
            gc.collect()

        if in_thread is True:
            callback = functools.partial(
                publish_function, content, is_json, headers)
            ServiceRabbitMQ.connection.add_callback_threadsafe(callback)
        else:
            publish_function(content, is_json, headers)

    @staticmethod
    def ack_message(delivery_tag):
        "Ask message"

        def ack_function():
            if ServiceRabbitMQ.channel.is_open:
                ServiceRabbitMQ.channel.basic_ack(delivery_tag)

        callback = functools.partial(ack_function)
        ServiceRabbitMQ.connection.add_callback_threadsafe(callback)
