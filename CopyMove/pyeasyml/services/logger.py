"""
Logger
"""

import sys
import logging
import inspect


class ServiceLogger:
    "Manipulate Logger"

    @staticmethod
    def init():
        "Init"

        formatter = logging.Formatter(
            '%(levelname) -10s %(name) -40s %(funcName) -40s %(lineno) -7d: %(custom_message)s')

        handler = logging.StreamHandler(sys.stdout)
        handler.setFormatter(formatter)

        # disable log library
        for lib in ['pika', 'boto', 'urllib3', 's3transfer', 'boto3', 'botocore', 'nose']:
            logging.getLogger(lib).propagate = False

        ServiceLogger.logger = logging.getLogger("app")

        ServiceLogger.active_trace = False
        ServiceLogger.add_trace()

        ServiceLogger.logger.addHandler(handler)
        ServiceLogger.logger.setLevel(logging.DEBUG)

    @staticmethod
    def add_trace(name=None):
        "Define the name of a trace that will be included in all log messages"

        ServiceLogger.trace = name

    @staticmethod
    def add_info():
        "finds the real info from where the logger was called"

        curframe = inspect.currentframe()
        calframe = inspect.getouterframes(curframe, 2)

        ServiceLogger.funcName = calframe[2][3]
        ServiceLogger.lineno = calframe[2][2]
        ServiceLogger.name = calframe[2][1]

        ServiceLogger.logger.addFilter(LoggerFilter())

    @staticmethod
    def error(*args, trace=False):
        ServiceLogger.active_trace = trace
        ServiceLogger.add_info()

        ServiceLogger.logger.error(*args)

    @staticmethod
    def info(*args, trace=False):
        ServiceLogger.active_trace = trace
        ServiceLogger.add_info()

        ServiceLogger.logger.info(*args)

    @staticmethod
    def debug(*args, trace=False):
        ServiceLogger.active_trace = trace
        ServiceLogger.add_info()

        ServiceLogger.logger.debug(*args)

    @staticmethod
    def critical(*args, trace=False):
        ServiceLogger.active_trace = trace
        ServiceLogger.add_info()

        ServiceLogger.logger.critical(*args)


class LoggerFilter(logging.Filter):
    "Filter Logger"

    def filter(self, record):

        record.funcName = ServiceLogger.funcName
        record.lineno = ServiceLogger.lineno
        record.name = ServiceLogger.name.split("/")[-1]

        message = record.getMessage()

        if ServiceLogger.trace and ServiceLogger.active_trace:
            message = "Trace=" + \
                ServiceLogger.trace + " " + message

        record.custom_message = message

        return True


ServiceLogger.init()
