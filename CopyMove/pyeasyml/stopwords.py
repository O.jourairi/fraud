
from . import datasets
from spacy.lang.fr.stop_words import STOP_WORDS as fr_stop

"""
-----------------------------------------------------------------
Desc:   init stopwords
Input:  n/a
Output: stopwords   

Traitements appliqués : 
- n/a 
-----------------------------------------------------------------
""" 
def init_stopwords():

    # init stop words from spacy
    stop_words = list(fr_stop)
    
    # get insee data 
    datasets.get_insee_datasets()   
    
    # extend stop words
    with open('noms.txt', 'r') as f:
        for line in f.readlines():
            stop_words.append(line[:-1])
    with open('prenoms.txt', 'r') as f:
        for line in f.readlines():
            stop_words.append(line[:-1])
    with open('communes.txt', 'r') as f:
        for line in f.readlines():
            stop_words.append(line[:-1])
    
    return stop_words    